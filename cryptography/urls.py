from django.urls import path
from . import views

urlpatterns = [
    path('encoder/', views.encoder),
    path('decoder/', views.decoder),
    path('results/', views.results),
    path('error/', views.error),
]