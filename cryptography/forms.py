from django import forms

class Input_form(forms.Form):
    input_text = forms.CharField(label="", help_text="", widget=forms.Textarea(attrs={'placeholder': 'Enter the message you want to process.'}))
    