from django.shortcuts import render
from .forms import Input_form
from .encode_msg import encode
from .decode_msg import decode
# Create your views here.
def encoder(request):
    if request.method == "GET":
        enc=Input_form()
        return render(request, 'cryptography/encoder.html', {'enc':enc})
    else:
        enc = Input_form(request.POST)
        if enc.is_valid():
            message = enc.cleaned_data["input_text"]
            result = encode(message)
            if result == 'error':
                return render(request, 'cryptography/error.html')
            return render(request, 'cryptography/results.html', {'result':result})
    

def decoder(request):
    if request.method == "GET":
        dec=Input_form()
        return render(request, 'cryptography/decoder.html', {'dec':dec})
    else:
        dec = Input_form(request.POST)
        if dec.is_valid():
            message = dec.cleaned_data['input_text']
            result = decode(message)
            if result == 'error':
                return render(request, 'cryptography/error.html')
            return render(request, 'cryptography/results.html',  {'result':result})
    

def results(request):
    return render(request, 'cryptography/results.html')

def error(request):
    return render(request, 'cryptography/error.html')